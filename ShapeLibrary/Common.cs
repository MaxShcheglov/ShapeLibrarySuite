﻿
namespace ShapeLibrary
{
    internal class Common
    {
        static public bool IsValid(double value)
        {
            return (value > 0) && (value != double.PositiveInfinity);
        }
    }
}

﻿using System;

namespace ShapeLibrary
{
    public class Rectangle : IShape
    {
        private double mWidth;
        private double mHeight;

        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
        }

        public double Width
        {
            get
            {
                return mWidth;
            }
            set
            {
                if (Common.IsValid(value))
                {
                    mWidth = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }
        public double Height
        {
            get
            {
                return mHeight;
            }
            set
            {
                if (Common.IsValid(value))
                {
                    mHeight = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        public double Area()
        {
            return GetArea(Width, Height);
        }
        public double Perimeter()
        {
            return GetPerimeter(Width, Height);
        }

        private double GetPerimeter(double a, double b)
        {
            return (a + b) * 2;
        }
        private double GetArea(double a, double b)
        {
            return a * b;
        }

        public override string ToString()
        {
            return String.Format("rectangle [width: {0}, height: {1}]",
                Width, Height);
        }
    }
}

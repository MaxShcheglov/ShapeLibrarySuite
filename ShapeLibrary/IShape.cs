﻿
namespace ShapeLibrary
{
    public interface IShape
    {
        double Area();
        double Perimeter();
    }
}

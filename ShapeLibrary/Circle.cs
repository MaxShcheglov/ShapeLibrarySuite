﻿using System;

namespace ShapeLibrary
{
    public class Circle : IShape
    {
        private double mRadius;

        public Circle(double radius)
        {
            Radius = radius;
        }

        public double Radius
        {
            get
            {
                return mRadius;
            }
            set
            {
                if (Common.IsValid(value))
                {
                    mRadius = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        public double Area()
        {
            return GetArea(Radius);
        }
        public double Perimeter()
        {
            return GetPerimeter(Radius);
        }

        private double GetPerimeter(double r)
        {
            return 2 * Math.PI * r;
        }
        private double GetArea(double r)
        {
            return Math.PI * Math.Pow(r, 2);
        }

        public override string ToString()
        {
            return String.Format("circle [radius: {0}]",
                Radius);
        }
    }
}
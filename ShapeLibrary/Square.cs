﻿using System;

namespace ShapeLibrary
{
    public class Square : IShape
    {
        private double mSide;

        public Square(double side)
        {
            Side = side;
        }

        public double Side
        {
            get
            {
                return mSide;
            }
            set
            {
                if (Common.IsValid(value))
                {
                    mSide = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        public double Area()
        {
            return GetArea(Side);
        }
        public double Perimeter()
        {
            return GetPerimeter(Side);
        }

        private double GetPerimeter(double a)
        {
            return a * 4;
        }
        private double GetArea(double a)
        {
            return a * a;
        }

        public override string ToString()
        {
            return String.Format("square [side: {0}]",
                Side);
        }
    }
}

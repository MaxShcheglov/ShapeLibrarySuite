﻿using System;

namespace ShapeLibrary
{
    public class Triangle : IShape
    {
        private double mSideA;
        private double mSideB;
        private double mSideC;

        public Triangle(double sideA, double sideB, double sideC)
        {
            if (Common.IsValid(sideA) && Common.IsValid(sideB) && Common.IsValid(sideC) && 
                CheckTriangleInequality(sideA, sideB, sideC))
            {
                mSideA = sideA;
                mSideB = sideB;
                mSideC = sideC;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public double SideA
        {
            get
            {
                return mSideA;
            }
            set
            {
                if (Common.IsValid(value) && CheckTriangleInequality(value, SideB, SideC))
                {
                    mSideA = value;
                }
                else
                {

                    throw new ArgumentException();
                }
            }
        }
        public double SideB
        {
            get
            {
                return mSideB;
            }
            set
            {
                if (Common.IsValid(value) && CheckTriangleInequality(SideA, value, SideC))
                {
                    mSideB = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }
        public double SideC
        {
            get
            {
                return mSideC;
            }
            set
            {
                if (Common.IsValid(value) && CheckTriangleInequality(SideA, SideB, value))
                {
                    mSideC = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        public double Area()
        {
            return GetArea(SideA, SideB, SideC);
        }
        public double Perimeter()
        {
            return GetPerimeter(SideA, SideB, SideC);
        }

        private double GetPerimeter(double a, double b, double c)
        {
            return a + b + c;
        }
        private double GetArea(double a, double b, double c)
        {
            double p = (a + b + c) / 2;
            double area = Math.Sqrt(p * (p - a) * (p - b) * (p - c));

            return area;
        }

        private bool CheckTriangleInequality(double a, double b, double c)
        {
            return (a + b > c) && (b + c > a) && (c + a > b);
        } 

        public override string ToString()
        {
            return String.Format("triangle [a side: {0}, b side: {1}, side c: {2}]",
                SideA, SideB, SideC);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using ShapeLibrary;

namespace UsingShapeLibraryApplication
{
    class Program
    {

        enum ShapeIndexEnum
        {
            Square = 0,
            Rectangle,
            Circle,
            Triangle
        }

        static void Main(string[] args)
        {
            List<IShape> shapes = new List<IShape>
            {
                new Square(10.0),
                new Rectangle(10.0, 15.0),
                new Circle(20.0),
                new Triangle(15.0, 25.0, 30.0)
            };

            ShowShapes(shapes);

            try
            {
                Console.WriteLine("attempt to change rectangle's width to 15");
                ((Rectangle)shapes[(int)ShapeIndexEnum.Rectangle]).Width = 15;
                Console.WriteLine("rectangle's width changed to 15 successfully");
            }
            catch(Exception e)
            {
                Console.WriteLine("can't change rectangle's width: {0}", e.GetType());
            }

            ShowShapes(shapes);

            try
            {
                Console.WriteLine("attempt to change triangle's second side to 10");
                ((Triangle)shapes[(int)ShapeIndexEnum.Triangle]).SideB = 10;
                Console.WriteLine("triangle's second side changed to 10 successfully");
            }
            catch(Exception e)
            {
                Console.WriteLine("can't change triangle's side B: {0}", e.GetType());
            }

            ShowShapes(shapes);

            WaitForAnyKeyPressed();
        }

        static void ShowShapes(List<IShape> shapes)
        {
            Console.WriteLine();
            foreach(IShape shape in shapes)
            {
                Console.WriteLine(
                    shape.ToString() +
                    string.Format(" area: {0}, perimeter: {1}", shape.Area(), shape.Perimeter())
                );
            }
            Console.WriteLine();
        }

        static void WaitForAnyKeyPressed()
        {
            Console.ReadKey();
        }
    }
}

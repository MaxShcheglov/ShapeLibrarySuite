﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeLibrary;

namespace ShapeLibraryTests
{
    [TestClass]
    public class ShapeLibraryTests
    {

        #region Square Tests

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SquareCreation_ArgumentIsPositiveInfinity_ExceptionThrown()
        {
            double side = 1.0 / 0.0;

            var square = new Square(side);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SquareCreation_ArgumentIsZero_ExceptionThrown()
        {
            double side = 0.0;

            var square = new Square(side);
        }

        [TestMethod]
        public void SquareCreation_ArgumentsAreValid_SquareCreated()
        {
            double side = 10.0;

            var square = new Square(side);

            Assert.AreEqual(side, square.Side);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SquarePropertySetting_ArgumentIsPositiveInfinity_ExceptionThrown()
        {
            double side = 10.0;

            var square = new Square(side);

            square.Side = 1.0 / 0.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SquarePropertySetting_ArgumentIsZero_ExceptionThrown()
        {
            double side = 10.0;

            var square = new Square(side);

            square.Side = 0.0;
        }

        [TestMethod]
        public void SquarePropertySetting_ArgumentIsValid_PropertyChanged()
        {
            double side = 10.0;

            var square = new Square(side);

            square.Side = 15.0;

            Assert.AreEqual(15.0, square.Side);
        }

        [TestMethod]
        public void SquareAreaCalculation_ExpectedAreaIs100_AreaIsCorrect()
        {
            double side = 10.0;
            double expected = 100.0;
            double delta = Math.Pow(10, -1);
            var square = new Square(side);

            double actual = square.Area();

            Assert.AreEqual(expected, actual, delta);
        }

        [TestMethod]
        public void SquarePerimeterCalculation_ExpectedPerimeterIs40_PerimeterIsCorrect()
        {
            double side = 10.0;
            double expected = 40.0;
            double delta = Math.Pow(10, -1);
            var square = new Square(side);

            double actual = square.Perimeter();

            Assert.AreEqual(expected, actual, delta);
        }

        #endregion

        #region Rectangle Tests

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void RectangleCreation_WidthIsPositiveInfinity_ExceptionThrown()
        {
            double width = 1.0 / 0.0;
            double height = 15.0; 

            var rectangle = new Rectangle(width, height);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void RectangleCreation_HeightIsZero_ExceptionThrown()
        {
            double width = 10.0;
            double height = 0.0;

            var rectangle = new Rectangle(width, height);
        }

        [TestMethod]
        public void RectangleCreation_ArgumentsIsValid_SquareCreated()
        {
            double width = 10.0;
            double height = 15.0;

            var rectangle = new Rectangle(width, height);

            Assert.AreEqual(width, rectangle.Width);
            Assert.AreEqual(height, rectangle.Height);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void RectanglePropertySetting_WidthIsPositiveInfinity_ExceptionThrown()
        {
            double width = 10.0;
            double height = 15.0;

            var rectangle = new Rectangle(width, height);

            rectangle.Height = 1.0 / 0.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void RectanglePropertySetting_HeightIsZero_ExceptionThrown()
        {
            double width = 10.0;
            double height = 15.0;

            var rectangle = new Rectangle(width, height);

            rectangle.Height = 0.0;
        }

        [TestMethod]
        public void RectanglePropertySetting_WidthIsValid_PropertyChanged()
        {
            double width = 10.0;
            double height = 15.0;

            var rectangle = new Rectangle(width, height);

            rectangle.Width = 11.0;

            Assert.AreEqual(11.0, rectangle.Width);
        }

        [TestMethod]
        public void RectangleAreaCalculation_ExpectedAreaIs150_AreaIsCorrect()
        {
            double width = 10.0;
            double height = 15.0;
            double expected = 150.0;
            double delta = Math.Pow(10, -1);
            var rectangle = new Rectangle(width, height);

            double actual = rectangle.Area();

            Assert.AreEqual(expected, actual, delta);
        }

        [TestMethod]
        public void RectanglePerimeterCalculation_ExpectedPerimeterIs50_PerimeterIsCorrect()
        {
            double width = 10.0;
            double height = 15.0;
            double expected = 50.0;
            double delta = Math.Pow(10, -1);
            var rectangle = new Rectangle(width, height);

            double actual = rectangle.Perimeter();

            Assert.AreEqual(expected, actual, delta);
        }

        #endregion

        #region Circle Tests

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CircleCreation_RadiusIsPositiveInfinity_ExceptionThrown()
        {
            double radius = 1.0 / 0.0;

            var circle = new Circle(radius);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CircleCreation_RadiusIsZero_ExceptionThrown()
        {
            double radius = 0.0;

            var circle = new Circle(radius);
        }

        [TestMethod]
        public void CircleCreation_RadiusIsValid_SquareCreated()
        {
            double radius = 20.0;

            var circle = new Circle(radius);

            Assert.AreEqual(radius, circle.Radius);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CirclePropertySetting_RadiusIsPositiveInfinity_ExceptionThrown()
        {
            double radius = 20.0;

            var circle = new Circle(radius);

            circle.Radius = 1.0 / 0.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CirclePropertySetting_ArgumentIsZero_ExceptionThrown()
        {
            double radius = 20.0;

            var circle = new Circle(radius);

            circle.Radius = 1.0 / 0.0;
        }

        [TestMethod]
        public void CirclePropertySetting_ArgumentIsValid_PropertyChanged()
        {
            double radius = 20.0;

            var circle = new Circle(radius);

            circle.Radius = 30.0;

            Assert.AreEqual(30.0, circle.Radius);
        }

        [TestMethod]
        public void CircleAreaCalculation_ExpectedAreaIs1256_AreaIsCorrect()
        {
            double radius = 20.0;
            double expected = 1256.63706143592;
            double delta = Math.Pow(10, -11);
            var circle = new Circle(radius);

            double actual = circle.Area();

            Assert.AreEqual(expected, actual, delta);
        }

        [TestMethod]
        public void CirclePerimeterCalculation_ExpectedPerimeterIs125_PerimeterIsCorrect()
        {
            double radius = 20.0;
            double expected = 125.663706143592;
            double delta = Math.Pow(10, -11);
            var circle = new Circle(radius);

            double actual = circle.Perimeter();

            Assert.AreEqual(expected, actual, delta);
        }

        #endregion

        #region Triangle Tests

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TriangleCreation_1stArgumentIsPositiveInfinity_ExceptionThrown()
        {
            double sideA = 1.0 / 0.0;
            double sideB = 15.0;
            double sideC = 9.0;

            var triangle = new Triangle(sideA, sideB, sideC);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TriangleCreation_2ndArgumentViolatesTriangleInequality_ExceptionThrown()
        {
            double sideA = 10;
            double sideB = 20.0;
            double sideC = 9.0;

            var triangle = new Triangle(sideA, sideB, sideC);
        }

        [TestMethod]
        public void TriangleCreation_ArgumentsAreValid_TriangleCreated()
        {
            double sideA = 10.0;
            double sideB = 15.0;
            double sideC = 9.0;

            var triangle = new Triangle(sideA, sideB, sideC);

            Assert.AreEqual(sideA, triangle.SideA);
            Assert.AreEqual(sideB, triangle.SideB);
            Assert.AreEqual(sideC, triangle.SideC);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TrianglePropertySetting_1stArgumentIsPositiveInfinity_ExceptionThrown()
        {
            double sideA = 10.0;
            double sideB = 15.0;
            double sideC = 9.0;

            var triangle = new Triangle(sideA, sideB, sideC);

            triangle.SideA = 1.0 / 0.0;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TrianglePropertySetting_2ndArgumentViolatesTriangleInequality_ExceptionThrown()
        {
            double sideA = 10.0;
            double sideB = 15.0;
            double sideC = 9.0;

            var triangle = new Triangle(sideA, sideB, sideC);

            triangle.SideB = 20.0;
        }

        [TestMethod]
        public void TrianglePropertySetting_ArgumentsAreValid_PropertyChanged()
        {
            double sideA = 10.0;
            double sideB = 15.0;
            double sideC = 9.0;

            var triangle = new Triangle(sideA, sideB, sideC);

            triangle.SideA = 11.0;
            triangle.SideB = 14.0;
            triangle.SideC = 10.0;

            Assert.AreEqual(11.0, triangle.SideA);
            Assert.AreEqual(14.0, triangle.SideB);
            Assert.AreEqual(10.0, triangle.SideC);
        }

        [TestMethod]
        public void TriangleAreaCalculation_ExpectedAreaIs187_AreaIsCorrect()
        {
            double sideA = 15.0;
            double sideB = 25.0;
            double sideC = 30.0;
            double expected = 187.082869338697;
            double delta = Math.Pow(10, -10);
            var triangle = new Triangle(sideA, sideB, sideC);
            
            double actual = triangle.Area();
            
            Assert.AreEqual(expected, actual, delta);
        }

        [TestMethod]
        public void TrianglePerimeterCalculation_ExpectedPerimeterIs70_PerimeterIsCorrect()
        {
            double sideA = 15.0;
            double sideB = 25.0;
            double sideC = 30.0;
            double expected = 70.0;
            double delta = Math.Pow(10, -10);
            var triangle = new Triangle(sideA, sideB, sideC);

            double actual = triangle.Perimeter();

            Assert.AreEqual(expected, actual, delta);
        }

        #endregion

    }
}
